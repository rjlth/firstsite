from django.shortcuts import render
from django.views import generic

from .models import Account, Subscriber, Relation

# Create your views here.

class IndexView(generic.ListView):
    template_name = 'instagram/index.html'
    context_object_name = 'account_list'

    def get_queryset(self):
        return Account.objects.all()

class DetailView(generic.ListView):
    template_name = 'instagram/detail.html'
    context_object_name = 'subscribers'

    def get_queryset(self):
        return Subscriber.objects.get()
