from django.db import models


# Create your models here.
class Account(models.Model):
    username = models.CharField(max_length=255)


class Subscriber(models.Model):
    username = models.CharField(max_length=255)


class Relation(models.Model):
    account = models.ForeignKey(Account)
    subscriber = models.ForeignKey(Subscriber)
