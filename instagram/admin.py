from django.contrib import admin

# Register your models here.
from .models import Account, Subscriber, Relation

admin.site.register(Account)